import pygame
import pygame_gui
import imageio
from random import randint

# Création de la fenêtre et son nom
pygame.init()
fenetre = pygame.display.set_mode((1200, 650))
pygame.display.set_caption('Rookie Investment')

# Création d'un gestionnaire d'interface (pygame GUI)
manager_ui = pygame_gui.UIManager((1200, 650))

class Joueur:
    def __init__(self):
        # Prix de départ 100000 euros
        self.fond = 100000

    # Met à jour les fonds
    def fond_update(self, n : int):
        self.fond = self.fond + n

class Entreprise:

    def __init__(self, Nom: str, variation: int, prod: str, action: int, note: int, description: str):
        # Initialisation de la description des entreprises
        self.nom_entreprise = Nom
        self.taux_variation = variation
        self.produit = prod
        self.prix_action = action
        self.note = note
        self.description = description

        # Chargement des événements mensuels
        self.even_negative = ("Suite à un manque de fonds, l'entreprise baisse le prix de ces produits, arrivera-t-elle à surmonter cette catastrophe?",
                              "Un grand nombre d'employé a été licencié pour nonchalance, mais est-ce là la solution pour surmonter la difficulté que rencontre l'entreprise?.",
                              "Des déclarations négatives sur l'entreprise a été publié sur les réseaux, l'entreprise a alors organisé une conférence, parviendront-ils à se justifier?",
                              "L'entreprise a de nouveaux concurrents sur le marché, arrivera-t-elle à suivre la cadence",
                              "L'entreprise se retrouve avec un nouveau PDG, des cadres supérieurs ne le porte pas bien à coeur, arrvera-t-il tout de même à mener l'entreprise vers la réussite? ",
                              "Au bord de la faillite, cette dernière fait un pari, en investissant dans un projet, déclaré comme perdu d'avance. Parviendra-t-elle à transformer ce desastre en réussite? "
                              )

        self.even_positive = ("L'entreprise réussi à devenir le N°1 du pays dans leur domaine, que lui arrivera-t-elle par la suite?.",
                              "L'entreprise, au bord de la faillite, a réussi à se redresser grâce à l'intervention du PDG ****, est-ce là le tournant vers le succès ou le désastre ?  ",
                              "L'édition du nouveau produit de l'entreprise est très attendue dans le monde, parviendra-t-elle à satisfaire les attentes des fans?",
                              "Collaboration avec l'entreprise **** réputée pour l'excellence de leur travail, le produit connaîtra-t-elle un succès? ",
                              "L'entreprise a été rachetée par un grand Groupe multinational, mais dans quel but?",
                              "L'entreprise lance le plus grand recrutement de tout les temps pour un projet, mais qu'arrivera-t-il si le projet est infructueux? "
                              )

    # RECURSIVITE: Choix des evenements négatifs
    def choix_event_negatif(self, Journal_negatif: list):
        n = randint(0, 5)

        if len(Journal_negatif) == 2:
            return Journal_negatif

        else:
            event = self.even_negative[n]
            if event not in Journal_negatif:
                Journal_negatif.append(event)
            return self.choix_event_negatif(Journal_negatif)

    # RECURSIVITE: Choix des evenements positifs
    def choix_event_positif(self, Journal_positif: list):
        n = randint(0, 5)

        if len(Journal_positif) == 2:
            return Journal_positif
        else:
            event = self.even_positive[n]
            if event not in Journal_positif:
                Journal_positif.append(event)

            return self.choix_event_positif(Journal_positif)

    # RECURSIVITE: Sélectionne au hasard 4 entreprises qui rencontreront un événement
    def choix_entreprise(self, Liste_entreprise_selectionne: list, Liste_entreprise_total: list):
        n = randint(0, 5)

        if len(Liste_entreprise_selectionne) == 4:
            return Liste_entreprise_selectionne
        else:
            entreprise = Liste_entreprise_total[n]
            if entreprise not in Liste_entreprise_selectionne:
                Liste_entreprise_selectionne.append(entreprise)
            return self.choix_entreprise(Liste_entreprise_selectionne, Liste_entreprise_total)

    # METHODE: Attribution d'un événement à une entreprise
    def combinaison_evenement_entreprise(self, Journal_negatif: list, Journal_positif: list, Liste_entreprise: list):
        Journal_1 = Liste_entreprise[0].nom_entreprise + ":" + Journal_negatif[0]
        Liste_entreprise[0].taux_variation = 90
        Journal_2 = Liste_entreprise[1].nom_entreprise + ":" + Journal_negatif[1]
        Liste_entreprise[1].taux_variation = 90
        Journal_3 = Liste_entreprise[2].nom_entreprise + ":" + Journal_positif[0]
        Liste_entreprise[2].taux_variation = 60
        Journal_4 = Liste_entreprise[3].nom_entreprise + ":" + Journal_positif[1]
        Liste_entreprise[3].taux_variation = 60

        return Journal_1, Journal_2, Journal_3, Journal_4

    # METHODE: Détermine le taux de variation du mois-ci de charque entreprise
    def variation_entreprise(self, Entreprise : object):
        variation = round(randint(20, Entreprise.taux_variation)*0.01,2)
        Entreprise.taux_variation = variation


    # METHODE: Définis au hasard si la somme investie augmentera de x% ou diminuera de x%
    def variation_somme_investie(self, somme_investie : int, Entreprise : object):
        self.n = randint(0, 1)
        if self.n == 0:
            somme_investie += somme_investie * Entreprise.taux_variation
        elif self.n == 1:
            somme_investie -= somme_investie * Entreprise.taux_variation

        return somme_investie

    # METHODE: Attribution des courbes selon les NEWS à chaque entreprise
    def courbe_variation_entreprise(self, Entreprise : object, x : int, y : int):
        # Condition si taux variation est de 61%-90%, attribution d'une courbe rouge à l'entreprise
        if 0.61 <= Entreprise.taux_variation <= 0.9:
            courbe = pygame.image.load("./image/courbe90%.png").convert()
            courbe_redimensionne = pygame.transform.scale(courbe, (30, 30))
            fenetre.blit(courbe_redimensionne, (x, y))

            # Condition si taux variation est de 31%-60%, attribution d'une courbe orange à l'entreprise
        elif 0.31 <= Entreprise.taux_variation <= 0.6:
            courbe = pygame.image.load("./image/courbe60%.png").convert()
            courbe_redimensionne = pygame.transform.scale(courbe, (30, 30))
            fenetre.blit(courbe_redimensionne, (x, y))

            # Condition si taux variation est de 20%-30%, attribution d'une courbe verte à l'entreprise
        else:
            courbe = pygame.image.load("./image/courbe30%.png").convert()
            courbe_redimensionne = pygame.transform.scale(courbe, (30, 30))
            fenetre.blit(courbe_redimensionne, (x, y))


class System:
    # Initialisation des dictionnaires de boutons et sliders
    def __init__(self):
        self.bouton = []
        self.slider_dico = {}
        self.detail_dico = {}

    # CRÉATION d'un bouton
    def creer_bouton(self, x : int, y : int, largeur : int, hauteur : int, couleur_transparence : tuple, texte : str, couleur_texte : tuple, police :str):

        # Initialisation des coordonnées du bouton
        self.x, self.y, self.largeur, self.hauteur = x, y, largeur, hauteur

        # Création de la surface du bouton transparent
        bouton_surface = pygame.Surface((largeur, hauteur), pygame.SRCALPHA)
        pygame.draw.rect(bouton_surface, couleur_transparence, (0, 0, largeur, hauteur))

        # DÉFINITR le contenu et les caractéristiques du bouton
        FONT_BUTTON = pygame.font.SysFont(police, 36)
        TEXT_BUTTON = FONT_BUTTON.render(texte, True, couleur_texte)

        # EMPLACEMENT du texte au centre du bouton
        TEXT_BOUTON_POSITION = TEXT_BUTTON.get_rect(center=(x + largeur / 2,
                                                     y + hauteur / 2))

        # AFFICHE le bouton dans la fenêtre
        fenetre.blit(bouton_surface, (x, y))  # la surface du bouton
        fenetre.blit(TEXT_BUTTON, TEXT_BOUTON_POSITION)  # le contenu du bouton

        # STOCK le bouton, sous forme de dictionnaire, dans la liste bouton
        self.bouton.append({
            'rect': pygame.Rect(x, y, largeur, hauteur),
            'texte': texte
        })

    # METHODE: Definis les boutons permettant de surfer entre les interfaces (tout en maintenant le bon fonctionnement du systeme)
    def check_bouton_if_click(self, x_souris : int, y_souris : int):
        for bouton in self.bouton:
            # Vérifie si la position du curseur est égale à cel du bouton
            if bouton['rect'].collidepoint(x_souris, y_souris):

                # Condition pour faire la distinction entre les différents boutons (nom des boutons + interface où on est)
                if bouton['texte'] == "PLAY" and jeu.interface_actuelle == "Accueille":
                    jeu.Interface_Principal()


                if bouton['texte'] == "NEWS" and jeu.interface_actuelle == "interface principal":
                    # Supprime les boutons détails d'entreprises après avoir changé d'interface
                    for detail in jeu.systeme.detail_dico:
                        detail.kill()

                    # Vide le dictionnaire de bouton survolable
                    jeu.systeme.detail_dico = {}

                    jeu.Interface_NEWS()


                if bouton['texte'] == 'retour' and jeu.interface_actuelle == "NEWS":
                    jeu.Interface_Principal()


                if bouton['texte'] == "START TRADE" and jeu.interface_actuelle == "interface principal":
                    # Supprime les boutons détails (survolable) d'entreprise après avoir changer d'interface
                    for detail in jeu.systeme.detail_dico:
                        detail.kill()

                    # Vide le dictionnaire de bouton survolable
                    jeu.systeme.detail_dico = {}

                    # Reinitialisation des investissements réalisés quand l'interface d'investissement est pénétré
                    jeu.investissement_chine = 0
                    jeu.investissement_russie = 0
                    jeu.investissement_EU = 0
                    jeu.investissement_japon = 0
                    jeu.investissement_france = 0
                    jeu.investissement_RU = 0

                    # Passage à l'interface d'investissement
                    jeu.Interface_Invest()


                if bouton['texte'] == 'Retour' and jeu.interface_actuelle == "Investissement":
                    # supprimer les sliders après avoir changé d'interface
                    for slider in self.slider_dico.keys():
                        slider.kill()

                    # Vide le dictionnaire de sliders
                    self.slider_dico = {}
                    jeu.Interface_Principal()


                if bouton['texte'] == "CONFIRM" and jeu.interface_actuelle == "Investissement":
                    # CONDITION : Vérifie si l'utilisateur a assez d'argent pour investir
                    if sum(jeu.investissement) <= jeu.joueur.fond:
                        # supprimer les sliders de l'interface
                        for slider in self.slider_dico.keys():
                            slider.kill()

                        # Vide le dictionnaire de sliders
                        self.slider_dico = {}
                        jeu.Interface_Rendu()
                    else:
                        jeu.no_more_credit = "Pas assez de crédit, soyez moins gourmand"


    # METHODE: Création de sliders
    def creer_slider(self, x : int, y : int, width : int, height : int, name : str):
        slider_rect = pygame.Rect((x, y), (width, height))
        self.slider = pygame_gui.elements.UIHorizontalSlider(relative_rect=slider_rect,
                                                             start_value=0,
                                                             value_range=(0, 10),
                                                             manager=manager_ui)
        # Enregistre les sliders dans un dictionnaire
        self.slider_dico[self.slider] = {'barre_position_precedente': 0, 'barre_position': 0, 'entreprise': name}


    # METHODE: Création de bouton survolable
    def creer_boutton_survolable(self, x : int, y : int, width : int, height : int, id_d : int):
        detail_rect = pygame.Rect((x, y), (width, height))
        self.detail = pygame_gui.elements.UIButton(relative_rect=pygame.Rect(detail_rect),
                                                text='Détail',
                                                manager=manager_ui)
        # Enregistre les boutons survolables dans un dictionnaire
        self.detail_dico[self.detail] = {"id_d": id_d}


class Jeu:
    def __init__(self) -> None:
        # Initialisations des class principals
        self.systeme = System()
        self.joueur = Joueur()

        # Initialisation des indicateurs
        self.interface_actuelle = "Accueille"
        self.mois = 0
        self.liste_mois = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"]

        # Initialisation du temps d'affichage et du message d'alerte (pas assez d'argent à investir)
        self.no_more_credit = None
        self.temps_debut_affichage = None
        self.current_frame = 0

        # Initialisation de la somme investie dans chacune des entreprises par l'utilisateur
        self.investissement_chine = 0
        self.investissement_russie = 0
        self.investissement_EU = 0
        self.investissement_japon = 0
        self.investissement_france = 0
        self.investissement_RU = 0

        # Initialisation du dictionnaire de boutons détails et descriptions des entreprises (+ logo)
        self.logo_entreprise_Americaine = pygame.image.load("./image/logo_EU.png").convert()
        self.logo_entreprise_Japonaise = pygame.image.load("./image/logo_japon.png").convert()
        self.logo_entreprise_Chinoise = pygame.image.load("./image/logo_chine.webp").convert()
        self.logo_entreprise_Francaise = pygame.image.load("./image/logo_france.png").convert()
        self.logo_entreprise_Russe = pygame.image.load("./image/logo_russie.png").convert()
        self.logo_entreprise_Britannique = pygame.image.load("./image/logo_RU.png").convert()
        self.description_entreprise = None

        # Initialisation des listes stockant les event et entreprise qui apparaitrerons dans les NEWS et le taux de variation des entreprises
        self.Journal_negatif = []
        self.Journal_positif = []
        self.Liste_entreprise_selectionne = []
        self.journal_definie = None
        self.variation_definie = None

        # Initialisation des différentes entreprises (OBJETS) par la class Entreprise
        self.Entreprise_Americaine = Entreprise("Entreprise Americaine", 30, "Iphone", 10000, 5,"Connue pour ses\n"
                                                                                                "iphones de qualités,\n"
                                                                                                "50% de la pop° mondial\n"
                                                                                                "utilise cette marque")
        self.Entreprise_Japonais = Entreprise("Entreprise Japonais", 30, "Manga", 8000, 3,"Classée 15e dans\n"
                                                                                          "le classement mondial,\n"
                                                                                          "réputée pour leur\n"
                                                                                          "intrigue d'histoire")
        self.Entreprise_Chinoise = Entreprise("Entreprise Chinoise", 30, "Game", 5000, 4,"L'une des 3 plus\n"
                                                                                         "grandes entreprises \n"
                                                                                         "de Chine,à fait succès\n"
                                                                                         "grâce à un RPG")
        self.Entreprise_Francaise = Entreprise("Entreprise Francaise", 30, "Dessert", 9000, 3,"Fondée par ****,\n"
                                                                                              "réputé pour avoir \n"
                                                                                              "travaillé chez MIRAZUR,\n"
                                                                                              "+ décor très élégant")
        self.Entreprise_Russe = Entreprise("Entreprise Russe", 30, "blé", 6000, 4, "Présent depuis le XXe\n"
                                                                                   "siècle, leur blé est\n"
                                                                                   "d'une qualité inouïe\n"
                                                                                   "malgré le mauvais temps")
        self.Entreprise_Britannique = Entreprise("Entreprise Britannique", 30, "Film productor", 7000, 3, "Réalise des films\n"
                                                                                                          "historique et croyant\n"
                                                                                                          "mais refuse tout de\n"
                                                                                                          "la science fiction")

        # Enregistrer les entreprises dans une liste
        self.liste_entreprise_total = (self.Entreprise_Americaine,
                                       self.Entreprise_Japonais,
                                       self.Entreprise_Chinoise,
                                       self.Entreprise_Francaise,
                                       self.Entreprise_Russe,
                                       self.Entreprise_Britannique)

    def Interface_Acceuil(self):
        # Indicateur de l'emplacement actuelle où se trouve l'utilisateur
        self.interface_actuelle = "Accueille"

        # INTERFACE: fond d'écran de l'acceuil
        FOND_ecran_menu = fenetre.fill((100, 100, 100))

        # TEXTE: Ajout du Titre de l'interface
        FONT_MENU = pygame.font.Font("./police/tokyo_taiyaki/TokyoTaiyakiDEMO-Regular.otf",100)
        TEXTE_MENU = FONT_MENU.render("    Rookie\n"
                                      "Investment", True, "#b68f40")
        TEXTE_RECT_MENU = TEXTE_MENU.get_rect(center=(600, 175))

        # DESSINER le TEXTE
        fenetre.blit(TEXTE_MENU, TEXTE_RECT_MENU)

        # BOUTON: appel de la méthode pour créer un bouton (menant vers l'interface principal "Entreprises à investir")
        self.systeme.creer_bouton(460, 400, 250, 75, (255, 255, 255, 100), 'PLAY', (255, 255, 255), 'couriernew')

    def Interface_Principal(self):
        # Indicateur de l'emplacement actuelle où se trouve l'utilisateur
        self.interface_actuelle = "interface principal"

        # INTERFACE: fond d'écran de l'interface principal
        fond = fenetre.fill((30, 0, 50))

        # INTERFACE: Affiche le mois
        police_mois = pygame.font.SysFont(None, 35)
        self.texte_mois = police_mois.render(str(self.liste_mois[self.mois]), True, "#b68f40")
        fenetre.blit(self.texte_mois, (1070, 30))

        # INTERFACE: Affichage des crédit que l'utilisateur détient
        self.stockage_credit = pygame.Surface((160, 30), pygame.SRCALPHA)
        self.stockage_credit.fill((0, 0, 0))
        self.stockage_credit.set_alpha(130)
        fenetre.blit(self.stockage_credit, (900, 25))

        # Fond que détient le joueur
        police_credit = pygame.font.SysFont("couriernew", 25)
        self.texte_credit = police_credit.render(str(self.joueur.fond), True, (255,255,255))
        fenetre.blit(self.texte_credit, (938, 28))

        sacoche = pygame.image.load("./image/credit.png").convert()
        self.sacoche_redimensionne = pygame.transform.scale(sacoche, (22, 22))
        fenetre.blit(self.sacoche_redimensionne, (907, 30))


        # INTERFACE: Description de l'entreprise de l'utilisateur
        fond_second = pygame.Surface((300, 500), pygame.SRCALPHA)  # taille
        fond_second.fill((0, 0, 0))  # couleur
        fond_second.set_alpha(140)  # transparence
        fenetre.blit(fond_second, (10, 50))  # coordonnées

        # Charger le logo de l'entreprise du player
        logo_player = pygame.image.load("./image/Victory_Lose.png").convert()
        logo_player_redimensionne = pygame.transform.scale(logo_player, (300, 250))
        fenetre.blit(logo_player_redimensionne, (10, 50))

        # Description de notre entreprise
        description_entreprise_utilisateur = "    [ROOKIE COMPANY]\n" \
                                             "Votre destination de\n" \
                                             "confiance pour des\n" \
                                             "produits de qualité et\n" \
                                             "une expérience de \n" \
                                             "shopping exceptionnelle.\n" \
                                             "Découvrez une sélection\n" \
                                             "soigneusement\n" \
                                             "choisie qui allie style,\n" \
                                             "durabilité et service\n" \
                                             "client attentif."
        FONT_description_entreprise_utilisateur = pygame.font.SysFont('couriernew', 17)
        TEXTE_description_entreprise_utilisateur = FONT_description_entreprise_utilisateur.render(
            description_entreprise_utilisateur, True,
            (255, 255, 255))
        TEXTE_RECT_description_entreprise_utilisateur = TEXTE_description_entreprise_utilisateur.get_rect(
            center=(150, 430))

        fenetre.blit(TEXTE_description_entreprise_utilisateur, TEXTE_RECT_description_entreprise_utilisateur)


        # INTERFACE: Fond noir transparent (contenant les entreprise)
        fond_second = pygame.Surface((850, 500), pygame.SRCALPHA)

        fond_second.fill((0, 0, 0)) #couleur
        fond_second.set_alpha(140) #transparence
        fenetre.blit(fond_second, (320, 60))  # coordonnées

        # BOUTON: Bouton survolable, si survoler affiche la description de l'entreprise
        self.systeme.creer_boutton_survolable(350, 70, 100, 30, 1)
        self.systeme.creer_boutton_survolable(650, 70, 100, 30, 2)
        self.systeme.creer_boutton_survolable(950, 70, 100, 30, 3)
        self.systeme.creer_boutton_survolable(350, 320, 100, 30, 4)
        self.systeme.creer_boutton_survolable(650, 320, 100, 30, 5)
        self.systeme.creer_boutton_survolable(950, 320, 100, 30, 6)

        # CONDITION : AJOUT du logo de l'entreprise Américaine (si bouton détails non survolé)
        if self.logo_entreprise_Americaine is not None:
            logo_entreprise_Americaine_redimensionne = pygame.transform.scale(self.logo_entreprise_Americaine, (175,150))
            fenetre.blit(logo_entreprise_Americaine_redimensionne, (350, 120))

        # CONDITION : Affichage de la fenêtre de description de l'entreprise Américaine (si bouton survolée)
        else:
            fond_second = pygame.Surface((235, 200), pygame.SRCALPHA)  # taille

            fond_second.fill((0, 0, 0)) #couleur
            fond_second.set_alpha(90) #transparence
            fenetre.blit(fond_second, (330,105 )) #coordonnées

            FONT_description_entreprise = pygame.font.SysFont('couriernew', 17)
            TEXTE_description_entreprise = FONT_description_entreprise.render(self.description_entreprise, True,(255, 255, 255))
            TEXTE_RECT_description_entreprise = TEXTE_description_entreprise.get_rect(center=(450, 200))

            fenetre.blit(TEXTE_description_entreprise, TEXTE_RECT_description_entreprise)


        # CONDITION : AJOUT du logo de l'entreprise Japonaise (si bouton détails non survolé)
        if self.logo_entreprise_Japonaise is not None:
            logo_entreprise_Japonaise_redimensionne = pygame.transform.scale(self.logo_entreprise_Japonaise, (175,150))
            fenetre.blit(logo_entreprise_Japonaise_redimensionne, (650, 120))

        # CONDITION : Affichage de la fenêtre de description de l'entreprise Japonaise (si bouton survolée)
        else:
            fond_second = pygame.Surface((235, 200), pygame.SRCALPHA)  # taille

            fond_second.fill((0, 0, 0))  # couleur
            fond_second.set_alpha(90)  # transparence
            fenetre.blit(fond_second, (630, 105))  # coordonnées

            FONT_description_entreprise = pygame.font.SysFont('couriernew', 17)
            TEXTE_description_entreprise = FONT_description_entreprise.render(self.description_entreprise, True,(255, 255, 255))
            TEXTE_RECT_description_entreprise = TEXTE_description_entreprise.get_rect(center=(750, 200))

            fenetre.blit(TEXTE_description_entreprise, TEXTE_RECT_description_entreprise)


        # CONDITION : AJOUT du logo de l'entreprise Chinoise (si bouton détails non survolé)
        if self.logo_entreprise_Chinoise is not None:
            logo_entreprise_Chinoise_redimensionne = pygame.transform.scale(self.logo_entreprise_Chinoise, (175,150))
            fenetre.blit(logo_entreprise_Chinoise_redimensionne, (950, 120))

        # CONDITION : Affichage de la fenêtre de description de l'entreprise Chinoise (si bouton survolée)
        else:
            fond_second = pygame.Surface((235, 200), pygame.SRCALPHA)  # taille

            fond_second.fill((0, 0, 0))  # couleur
            fond_second.set_alpha(90)  # transparence
            fenetre.blit(fond_second, (930, 105))  # coordonnées

            FONT_description_entreprise = pygame.font.SysFont('couriernew', 17)
            TEXTE_description_entreprise = FONT_description_entreprise.render(self.description_entreprise, True,
                                                                              (255, 255, 255))
            TEXTE_RECT_description_entreprise = TEXTE_description_entreprise.get_rect(center=(1050, 200))

            fenetre.blit(TEXTE_description_entreprise, TEXTE_RECT_description_entreprise)


        # CONDITION : AJOUT du logo de l'entreprise Francaise (si bouton détails non survolé)
        if self.logo_entreprise_Francaise is not None:
            logo_entreprise_Francaise_redimensionne = pygame.transform.scale(self.logo_entreprise_Francaise, (175,150))
            fenetre.blit(logo_entreprise_Francaise_redimensionne, (350, 370))

        # CONDITION : Affichage de la fenêtre de description de l'entreprise Francaise (si bouton survolée)
        else:
            fond_second = pygame.Surface((235, 200), pygame.SRCALPHA)  # taille

            fond_second.fill((0, 0, 0))  # couleur
            fond_second.set_alpha(90)  # transparence
            fenetre.blit(fond_second, (330, 355))  # coordonnées

            FONT_description_entreprise = pygame.font.SysFont('couriernew', 17)
            TEXTE_description_entreprise = FONT_description_entreprise.render(self.description_entreprise, True,
                                                                              (255, 255, 255))
            TEXTE_RECT_description_entreprise = TEXTE_description_entreprise.get_rect(center=(450, 450))

            fenetre.blit(TEXTE_description_entreprise, TEXTE_RECT_description_entreprise)


        # CONDITION : AJOUT du logo de l'entreprise Russe (si bouton détails non survolé)
        if self.logo_entreprise_Russe is not None:
            logo_entreprise_Russe_redimensionne = pygame.transform.scale(self.logo_entreprise_Russe, (175,150))
            fenetre.blit(logo_entreprise_Russe_redimensionne, (650, 370))

        # CONDITION : Affichage de la fenêtre de description de l'entreprise Russe (si bouton survolée)
        else:
            fond_second = pygame.Surface((235, 200), pygame.SRCALPHA)  # taille

            fond_second.fill((0, 0, 0))  # couleur
            fond_second.set_alpha(90)  # transparence
            fenetre.blit(fond_second, (630, 355))  # coordonnées

            FONT_description_entreprise = pygame.font.SysFont('couriernew', 17)
            TEXTE_description_entreprise = FONT_description_entreprise.render(self.description_entreprise, True,
                                                                              (255, 255, 255))
            TEXTE_RECT_description_entreprise = TEXTE_description_entreprise.get_rect(center=(750, 450))

            fenetre.blit(TEXTE_description_entreprise, TEXTE_RECT_description_entreprise)


        # CONDITION : AJOUT du logo de l'entreprise Britannique (si bouton détails non survolé)
        if self.logo_entreprise_Britannique is not None:
            logo_entreprise_Britannique_redimensionne = pygame.transform.scale(self.logo_entreprise_Britannique, (175,150))
            fenetre.blit(logo_entreprise_Britannique_redimensionne, (950, 370))

        # CONDITION : Affichage de la fenêtre de description de l'entreprise Britannique (si bouton survolée)
        else:
            fond_second = pygame.Surface((235, 200), pygame.SRCALPHA)  # taille

            fond_second.fill((0, 0, 0))  # couleur
            fond_second.set_alpha(90)  # transparence
            fenetre.blit(fond_second, (930, 355))  # coordonnées

            FONT_description_entreprise = pygame.font.SysFont('couriernew', 17)
            TEXTE_description_entreprise = FONT_description_entreprise.render(self.description_entreprise, True,
                                                                              (255, 255, 255))
            TEXTE_RECT_description_entreprise = TEXTE_description_entreprise.get_rect(center=(1050, 450))

            fenetre.blit(TEXTE_description_entreprise, TEXTE_RECT_description_entreprise)


        # RELEVER (NEWS) : les événements et entreprises du MOIS
        if self.journal_definie is None:
            Journal_negatif_rempli = self.Entreprise_Chinoise.choix_event_negatif(self.Journal_negatif)

            Journal_positif_rempli = self.Entreprise_Chinoise.choix_event_positif(self.Journal_positif)

            Liste_entreprise_rempli = self.Entreprise_Chinoise.choix_entreprise(self.Liste_entreprise_selectionne,
                                                                            self.liste_entreprise_total)
            self.Journal_1, self.Journal_2, self.Journal_3, self.Journal_4 = self.Entreprise_Chinoise.combinaison_evenement_entreprise(Journal_negatif_rempli, Journal_positif_rempli, Liste_entreprise_rempli)

            self.journal_definie = "Les événements et entreprises ont été relevé"

        # RELEVE le taux de variation de chaque entreprise
        if self.variation_definie == None:
            self.Entreprise_Americaine.variation_entreprise(self.Entreprise_Americaine)
            self.Entreprise_Japonais.variation_entreprise(self.Entreprise_Japonais)
            self.Entreprise_Chinoise.variation_entreprise(self.Entreprise_Chinoise)
            self.Entreprise_Francaise.variation_entreprise(self.Entreprise_Francaise)
            self.Entreprise_Russe.variation_entreprise(self.Entreprise_Russe)
            self.Entreprise_Britannique.variation_entreprise(self.Entreprise_Britannique)
            self.variation_definie = "Les taux de variations de chaque entreprise ont été relevé"

        # AJOUT des courbes de variations pour chaque entreprise (varie de 30%,60%,90%) selon son taux de variation
        self.Entreprise_Americaine.courbe_variation_entreprise(self.Entreprise_Americaine, 470, 70)
        self.Entreprise_Japonais.courbe_variation_entreprise(self.Entreprise_Japonais, 770, 70)
        self.Entreprise_Chinoise.courbe_variation_entreprise(self.Entreprise_Chinoise, 1070, 70)
        self.Entreprise_Francaise.courbe_variation_entreprise(self.Entreprise_Francaise, 470, 320)
        self.Entreprise_Russe.courbe_variation_entreprise(self.Entreprise_Russe, 770, 320)
        self.Entreprise_Britannique.courbe_variation_entreprise(self.Entreprise_Britannique, 1070, 320)

        # BOUTON: Accéder au NEWS
        self.systeme.creer_bouton(210, 50, 100, 50, (255, 255, 255, 255), 'NEWS', (0, 0, 0), 'couriernew')

        # BOUTON: commencer à investir
        self.systeme.creer_bouton(500, 580, 250, 65, (0, 0, 0, 255), 'START TRADE', (255, 255, 255), 'couriernew')

    def Interface_NEWS(self):
        # Indicateur de l'emplacement actuelle où se trouve l'utilisateur
        self.interface_actuelle = "NEWS"

        # Initialisation des listes contenant les événements arrivés aux entreprises ce mois-ci
        self.texte_journal = []
        self.texte_journal_rect = []
        self.police_journal = pygame.font.SysFont('couriernew', 20)
        self.police_journal_titre = pygame.font.SysFont('couriernew', 30)

        # INTERFACE: Implémentation du fond d'écran de l'interface NEWS
        fond_NEWS = fenetre.fill((30, 0, 50))

        # INTERFACE: Fond noir contenant le résultat du mois (gain et perte)
        fond_second = pygame.Surface((1000, 550), pygame.SRCALPHA)
        # Calcul des coordonnées pour placer le fond_second au centre de l'écran
        x_fond_second = (fenetre.get_width() - fond_second.get_width()) // 2
        y_fond_second = (fenetre.get_height() - fond_second.get_height()) // 2

        fond_second.fill((0, 0, 0))
        fond_second.set_alpha(140)
        fenetre.blit(fond_second, (x_fond_second, y_fond_second))


        self.texte_journal.append(self.police_journal_titre.render("Journal Négative", True, "#b68f40", None, 200))
        self.texte_journal.append(self.police_journal_titre.render("Journal Positive", True, "#b68f40", None, 200))

        # TEXTE: NEWS info 1
        self.texte_journal.append(self.police_journal.render(self.Journal_1, True, "#b68f40", None, 350))
        # TEXTE: NEWS info 2
        self.texte_journal.append(self.police_journal.render(self.Journal_2, True, "#b68f40", None, 350))
        # TEXTE: NEWS info 3
        self.texte_journal.append(self.police_journal.render(self.Journal_3, True, "#b68f40", None, 350))
        # TEXTE: NEWS info 4
        self.texte_journal.append(self.police_journal.render(self.Journal_4, True, "#b68f40", None, 350))

        self.systeme.creer_bouton(950, 550, 150, 50, (255, 255, 255, 100), "retour", (0, 0, 0), None)

        # AFFICHER les actualités
        for i in range(6):
            if i < 2: # Coordonné des textes journals
                self.texte_journal_rect.append(self.texte_journal[i].get_rect(center=(300 , 210+i*250)))
                fenetre.blit(self.texte_journal[i], self.texte_journal_rect[i])
            elif i < 4: # Coordonné des textes journals négative
                self.texte_journal_rect.append(self.texte_journal[i].get_rect(center=(550 + 350 * (i-2), 210)))
                fenetre.blit(self.texte_journal[i], self.texte_journal_rect[i])
            else: # Coordonné des textes journals positive
                self.texte_journal_rect.append(self.texte_journal[i].get_rect(center=(550 + 350 * (i-4), 460)))
                fenetre.blit(self.texte_journal[i], self.texte_journal_rect[i])

        police_titre = pygame.font.SysFont(None, 40)
        texte_titre = police_titre.render("JOURNALS", True, "#b68f40")
        # Affichage du titre et du mois
        fenetre.blit(texte_titre, (350, 70))
        fenetre.blit(self.texte_mois, (520, 70))


    def Interface_Invest(self):
        # Indicateur de l'emplacement actuelle où se trouve l'utilisateur
        self.interface_actuelle = "Investissement"

        # INTERFACE: Fond d'écran de l'interface d'Investissement
        fond_investissement = fenetre.fill((30, 0, 50))

        # INTERFACE: Fond noir contenant les différents fonctionnalité pour investir (sliders)
        fond_second = pygame.Surface((1000, 500), pygame.SRCALPHA)

        # Calcul des coordonnées pour placer le fond_second au centre de l'écran
        x_fond_second = (fenetre.get_width() - fond_second.get_width()) // 2
        y_fond_second = (fenetre.get_height() - fond_second.get_height()) // 2

        fond_second.fill((0, 0, 0))
        fond_second.set_alpha(140)
        fenetre.blit(fond_second, (x_fond_second, y_fond_second))

        # INTERFACE: Affichage des crédit que l'utilisateur détient
        fenetre.blit(self.stockage_credit, (900, 25))
        fenetre.blit(self.texte_credit, (938, 28))
        fenetre.blit(self.sacoche_redimensionne, (907, 30))

        # INTERFACE: Affiche le mois
        fenetre.blit(self.texte_mois, (1070, 30))

        # BOUTON: création des sliders, représentant chacune une entrprise différente
        if not self.systeme.slider_dico:
            # Appel de la méthode pour créer un slider
            self.systeme.creer_slider(375, 140, 200, 20, "Etats-Unis")
            self.systeme.creer_slider(375, 310, 200, 20, "Japon")
            self.systeme.creer_slider(375, 480, 200, 20, "Chine")
            self.systeme.creer_slider(850, 140, 200, 20, "France")
            self.systeme.creer_slider(850, 310, 200, 20, "Russie")
            self.systeme.creer_slider(850, 480, 200, 20, "Royaume-Uni")

        # TEXTE: Affichage de la somme investie dans chacune des entreprises
        self.investissement = [self.investissement_EU, self.investissement_japon, self.investissement_chine,
                               self.investissement_france, self.investissement_russie,
                               self.investissement_RU]
        for i in range(0,6):
            if i <= 2:
                FONT1 = pygame.font.SysFont('couriernew', 25)
                TEXTE1 = FONT1.render(f"{self.investissement[i]}", True, "#b68f40")
                TEXTE_RECT1 = TEXTE1.get_rect(center=(410 , 120 + (i * 170)))
                fenetre.blit(TEXTE1, TEXTE_RECT1)
            else:
                FONT2 = pygame.font.SysFont('couriernew', 25)
                TEXTE2 = FONT2.render(f"{self.investissement[i]}", True, "#b68f40")
                TEXTE_RECT2 = TEXTE2.get_rect(center=(885, 120 +((i-3) * 170)))
                fenetre.blit(TEXTE2, TEXTE_RECT2)

        # IMAGE: Affichage des logos d'entreprise
        self.logo_entreprise_Americaine = pygame.image.load("./image/logo_EU.png").convert()
        logo_entreprise_Americaine_redimensionne = pygame.transform.scale(self.logo_entreprise_Americaine, (175, 150))
        fenetre.blit(logo_entreprise_Americaine_redimensionne, (175, 77))

        self.logo_entreprise_Japonaise = pygame.image.load("./image/logo_japon.png").convert()
        logo_entreprise_Japonaise_redimensionne = pygame.transform.scale(self.logo_entreprise_Japonaise, (175, 150))
        fenetre.blit(logo_entreprise_Japonaise_redimensionne, (175, 247))

        self.logo_entreprise_Chinoise = pygame.image.load("./image/logo_chine.webp").convert()
        logo_entreprise_Chinoise_redimensionne = pygame.transform.scale(self.logo_entreprise_Chinoise, (175, 150))
        fenetre.blit(logo_entreprise_Chinoise_redimensionne, (175, 417))

        self.logo_entreprise_Francaise = pygame.image.load("./image/logo_france.png").convert()
        logo_entreprise_Francaise_redimensionne = pygame.transform.scale(self.logo_entreprise_Francaise, (175, 150))
        fenetre.blit(logo_entreprise_Francaise_redimensionne, (650, 77))

        self.logo_entreprise_Russe = pygame.image.load("./image/logo_russie.png").convert()
        logo_entreprise_Russe_redimensionne = pygame.transform.scale(self.logo_entreprise_Russe, (175, 150))
        fenetre.blit(logo_entreprise_Russe_redimensionne, (650, 247))

        self.logo_entreprise_Britannique = pygame.image.load("./image/logo_RU.png").convert()
        logo_entreprise_Britannique_redimensionne = pygame.transform.scale(self.logo_entreprise_Britannique, (175, 150))
        fenetre.blit(logo_entreprise_Britannique_redimensionne, (650, 417))


        # BOUTON: Retour en arrière
        self.systeme.creer_bouton(10, 10, 150, 50, (0, 0, 0, 255), 'Retour', (255, 255, 255), 'couriernew')

        # BOUTON: Confirmation de la somme à investir
        self.systeme.creer_bouton(980, 580, 200, 65, (0, 0, 0, 255), 'CONFIRM', (255, 255, 255), 'couriernew')

        if self.no_more_credit is not None:

            if self.temps_debut_affichage is None:
                self.temps_debut_affichage = pygame.time.get_ticks()

            # Calculer le temps écoulé depuis le début de l'affichage du texte
            temps_actuel = pygame.time.get_ticks()
            temps_ecoule = temps_actuel - self.temps_debut_affichage

            # ALERTE INTERFACE: Affichage un message d'alerte (pas assez de crédit)
            fond_alerte = pygame.Surface((1200, 30), pygame.SRCALPHA)
            fond_alerte.fill((0, 0, 0))
            fond_alerte.set_alpha(200)

            FONT_alerte = pygame.font.SysFont("couriernew", 25)
            TEXTE_alerte = FONT_alerte.render(self.no_more_credit, True, (255,255,255))
            TEXTE_RECT_alerte = TEXTE_alerte.get_rect(center=(600, 205))

            if temps_ecoule < 2000: # 2000 millisecondes = 2 secondes
                # DESSINER le TEXTE et le fond
                fenetre.blit(fond_alerte, (0, 190))
                fenetre.blit(TEXTE_alerte, TEXTE_RECT_alerte)
            else:
                self.temps_debut_affichage = None
                self.no_more_credit = None

    def Interface_Rendu(self):
        # Indicateur de l'emplacement actuelle où se trouve l'utilisateur
        self.interface_actuelle = "Gain et Perte du mois"

        # INTERFACE: Fond d'écran de l'interface d'Investissement
        fond_gain_perte = fenetre.fill((30, 30, 150))

        # INTERFACE: Fond noir contenant le résultat du mois (gain et perte)
        fond_second = pygame.Surface((1000, 550), pygame.SRCALPHA)
        # Calcul des coordonnées pour placer le fond_second au centre de l'écran
        x_fond_second = (fenetre.get_width() - fond_second.get_width()) // 2
        y_fond_second = (fenetre.get_height() - fond_second.get_height()) // 2

        fond_second.fill((0, 0, 0))
        fond_second.set_alpha(140)
        fenetre.blit(fond_second, (x_fond_second, y_fond_second))

        # Initialisation des coordonnées
        self.x_fond_article = []
        self.y_fond_article = []
        self.fond_article = pygame.Surface((260, 220), pygame.SRCALPHA)

        # INTERFACE: Boucle pour créé différents fond avec différents coordonnées
        for i in range(6):
            # Placer le fond_article en haut du le fond second
            if i < 3:
                self.x_fond_article.append(160 + 300 * i)
                self.y_fond_article.append(115)
            # Placer le fond_article en bas du le fond second
            else:
                self.x_fond_article.append(160 + 300 * (i - 3))
                self.y_fond_article.append(350)

            # Afficher les fond_article
            self.fond_article.fill((255, 255, 255))
            self.fond_article.set_alpha(200)
            fenetre.blit(self.fond_article, (self.x_fond_article[i], self.y_fond_article[i]))

        # Définir le titre de l'interface
        police_titre = pygame.font.SysFont('couriernew', 35)
        texte_titre = police_titre.render("Simulated Market Trade Receipt", True, "#b68f40")

        # Affichage du titre et du mois
        fenetre.blit(texte_titre, (150, 50))
        fenetre.blit(self.texte_mois, (800, 60))

        # Initialisation
        self.ARGENT_TEXTE = []
        self.ARGENT_APRES_INVEST = []
        self.ARGENT_RECT = []
        self.Entreprise_tv = []
        self.FONT = pygame.font.SysFont('couriernew', 25)

        # CALCULER la somme après l'investissement (gain ou perte), l'enregistrer dans une liste ARGENT_APRES_INVEST
        self.ARGENT_APRES_INVEST.append(
            self.Entreprise_Americaine.variation_somme_investie(self.investissement[0],self.Entreprise_Americaine))
        self.ARGENT_APRES_INVEST.append(
            self.Entreprise_Japonais.variation_somme_investie(self.investissement[1], self.Entreprise_Japonais))
        self.ARGENT_APRES_INVEST.append(
            self.Entreprise_Chinoise.variation_somme_investie(self.investissement[2],self.Entreprise_Chinoise))
        self.ARGENT_APRES_INVEST.append(
            self.Entreprise_Francaise.variation_somme_investie(self.investissement[3],self.Entreprise_Francaise))
        self.ARGENT_APRES_INVEST.append(
            self.Entreprise_Russe.variation_somme_investie(self.investissement[4],self.Entreprise_Russe))
        self.ARGENT_APRES_INVEST.append(
            self.Entreprise_Britannique.variation_somme_investie(self.investissement[5],self.Entreprise_Britannique))

        # Affiche la somme investie et le rendu (somme après l'investissement))
        for i in range(6):
            self.ARGENT_TEXTE.append(
                self.FONT.render(f"Investie :{self.investissement[i]}\n={self.ARGENT_APRES_INVEST[i]}", True, "#b68f40", None,
                                 300))  # Enregistre les textes dans ARGENT_TEXTES

            # Condition pour placer les textes dans un ordres précis avec des coordonnées différents
            if i < 3:
                self.ARGENT_RECT.append(self.ARGENT_TEXTE[i].get_rect(center=(320 + (i * 300), 305)))
            else:
                self.ARGENT_RECT.append(self.ARGENT_TEXTE[i].get_rect(center=(320 + 300 * (i - 3), 535)))
            fenetre.blit(self.ARGENT_TEXTE[i], self.ARGENT_RECT[i])  # Afficher les textes
            self.joueur.fond_update(self.ARGENT_APRES_INVEST[i] - self.investissement[i])


        # IMAGE: Implémentation des logos d'entreprises
        self.logo_entreprise_Americaine = pygame.image.load("./image/logo_EU.png").convert()
        logo_entreprise_Americaine_redimensionne = pygame.transform.scale(self.logo_entreprise_Americaine, (175, 150))
        fenetre.blit(logo_entreprise_Americaine_redimensionne, (200, 120))

        self.logo_entreprise_Japonaise = pygame.image.load("./image/logo_japon.png").convert()
        logo_entreprise_Japonaise_redimensionne = pygame.transform.scale(self.logo_entreprise_Japonaise, (175, 150))
        fenetre.blit(logo_entreprise_Japonaise_redimensionne, (500, 120))

        self.logo_entreprise_Chinoise = pygame.image.load("./image/logo_chine.webp").convert()
        logo_entreprise_Chinoise_redimensionne = pygame.transform.scale(self.logo_entreprise_Chinoise, (175, 150))
        fenetre.blit(logo_entreprise_Chinoise_redimensionne, (800, 120))

        self.logo_entreprise_Francaise = pygame.image.load("./image/logo_france.png").convert()
        logo_entreprise_Francaise_redimensionne = pygame.transform.scale(self.logo_entreprise_Francaise, (175, 150))
        fenetre.blit(logo_entreprise_Francaise_redimensionne, (200, 355))

        self.logo_entreprise_Russe = pygame.image.load("./image/logo_russie.png").convert()
        logo_entreprise_Russe_redimensionne = pygame.transform.scale(self.logo_entreprise_Russe, (175, 150))
        fenetre.blit(logo_entreprise_Russe_redimensionne, (500, 355))

        self.logo_entreprise_Britannique = pygame.image.load("./image/logo_RU.png").convert()
        logo_entreprise_Britannique_redimensionne = pygame.transform.scale(self.logo_entreprise_Britannique, (175, 150))
        fenetre.blit(logo_entreprise_Britannique_redimensionne, (800, 355))

        # SYSTEME: Changement de page après 5 secondes
        pygame.display.flip()  # rafraichit l'écran
        pygame.time.delay(5000)  # attend 5 s

        self.mois += 1 # passage au mois suivant
        if self.mois < 12:
            self.interface_actuelle = "interface principal"  # Retour interface principal
            # Reinitialise le contenu du journal pour le mois suivant
            self.Journal_negatif = []
            self.Journal_positif = []
            self.Liste_entreprise_selectionne = []

            # Reinitialise les taux de variations des entreprises et actualités
            self.journal_definie = None
            self.variation_definie = None

            self.Entreprise_Americaine.taux_variation = 30
            self.Entreprise_Japonais.taux_variation = 30
            self.Entreprise_Chinoise.taux_variation = 30
            self.Entreprise_Francaise.taux_variation = 30
            self.Entreprise_Russe.taux_variation = 30
            self.Entreprise_Britannique.taux_variation = 30

        else:
            self.interface_actuelle = "END"

    def Interface_Finale(self):
        # Indicateur de l'emplacement actuelle où se trouve l'utilisateur
        self.interface_actuelle = "END"

        # Initialisation des messages de fin de partie
        self.message_victoire = "Après avoir atteint le Quota, vous avez continueé sur cette\n" \
                                "Lancée et êtes devenus la plus grosse entreprise de la région"

        self.message_defaite = "Malheureusement,vous n'avez pas atteint le Quota, votre entreprise a fait\n" \
                               "faillite, vous vous retrouvez endettés vous décidez alors de prendre la fuite"

        # CONDITION : si l'argent du joueur atteint le quota (500 000€) afficher interface vitoire sinon défaite
        if self.joueur.fond >= 400000:
            # INTERFACE: Fond d'écran de victoire
            interface_victoire = pygame.image.load("./image/Victory_Lose.png").convert()
            interface_victoire_redimensionne = pygame.transform.scale(interface_victoire,(fenetre.get_width(), fenetre.get_height()))
            fenetre.blit(interface_victoire_redimensionne, (0, 0))

            # INTERFACE : Bande noire contenant le message de fin de partie
            fond_victoire = pygame.Surface((1200, 150), pygame.SRCALPHA)
            fond_victoire.fill((0, 0, 0))
            fond_victoire.set_alpha(170)

            # INTERFACE: Message de fin de partie
            FONT_victoire = pygame.font.SysFont("couriernew", 50)
            TEXTE_victoire = FONT_victoire.render("VICTORY", True, (255, 255, 255))
            TEXTE_RECT_victoire = TEXTE_victoire.get_rect(center=(600, 225))

            FONT_victoire_info = pygame.font.SysFont("couriernew", 25)
            TEXTE_victoire_info = FONT_victoire_info.render(self.message_victoire, True, (255, 255, 255))
            TEXTE_RECT_victoire_info = TEXTE_victoire_info.get_rect(center=(600, 280))

            # DESSINER le TEXTE et le fond
            fenetre.blit(fond_victoire, (0, 190))
            fenetre.blit(TEXTE_victoire, TEXTE_RECT_victoire)
            fenetre.blit(TEXTE_victoire_info, TEXTE_RECT_victoire_info)

        else:
            interface_defaite = pygame.image.load("./image/Victory_Lose.png").convert()
            interface_defaite_redimensionne = pygame.transform.scale(interface_defaite, (fenetre.get_width(), fenetre.get_height()))
            fenetre.blit(interface_defaite_redimensionne, (0, 0))

            # INTERFACE : Bande noire contenant le message de fin de partie
            fond_defaite = pygame.Surface((1200, 150), pygame.SRCALPHA)
            fond_defaite.fill((0, 0, 0))
            fond_defaite.set_alpha(170)

            # INTERFACE: Message de fin de partie
            FONT_defaite = pygame.font.SysFont("couriernew", 50)
            TEXTE_defaite = FONT_defaite.render("DEFEAT", True, (255, 255, 255))
            TEXTE_RECT_defaite = TEXTE_defaite.get_rect(center=(600, 225))

            FONT_defaite_info = pygame.font.SysFont("couriernew", 25)
            TEXTE_defaite_info = FONT_defaite_info.render(self.message_defaite, True, (255, 255, 255))
            TEXTE_RECT_defaite_info = TEXTE_defaite_info.get_rect(center=(600, 280))

            # DESSINER le TEXTE et le fond
            fenetre.blit(fond_defaite, (0, 190))
            fenetre.blit(TEXTE_defaite, TEXTE_RECT_defaite)
            fenetre.blit(TEXTE_defaite_info, TEXTE_RECT_defaite_info)

# Instanciation de la classe Jeu
jeu = Jeu()