import sys
from fonction import*

# Initialise le temps
clock = pygame.time.Clock()

# Boucle principale
while True:
    #relève les différents événements, mouvements de l'utilisateur
    for event in pygame.event.get():

        # EVENEMENT : si l'utilisateur ferme la fenêtre
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        # EVENEMENT : si l'utilisateur effectue un mouvement horizontal avec le slider
        if event.type == pygame_gui.UI_HORIZONTAL_SLIDER_MOVED:
            # Spécification: les slider enregistrer dqns le dictionnaire
            if event.ui_element in jeu.systeme.slider_dico:

                # Relève les positions d'un slider scpécifique dans le dictionnaire slider
                slider_info = jeu.systeme.slider_dico[event.ui_element]
                slider_info['barre_position_precedente'] = slider_info['barre_position']
                slider_info['barre_position'] = event.value

                # Relève la différence entre la position initiale et précédente
                difference_position = abs(slider_info['barre_position'] - slider_info['barre_position_precedente'])
                distance_minimale = 1 # initialisation d'une distance minimale

                #CONDITION: augmente la valeur d'investissement si défilement de la barre vers la droite
                if difference_position >= distance_minimale and \
                slider_info['barre_position_precedente'] < slider_info['barre_position']:
                    # Vérifie quels sliders est déplacés
                    if slider_info['entreprise'] == "Etats-Unis":
                        jeu.investissement_EU += difference_position * jeu.Entreprise_Americaine.prix_action
                    if slider_info['entreprise'] == "Japon":
                        jeu.investissement_japon += difference_position * jeu.Entreprise_Japonais.prix_action
                    if slider_info['entreprise'] == "Chine":
                        jeu.investissement_chine += difference_position * jeu.Entreprise_Chinoise.prix_action
                    if slider_info['entreprise'] == "France":
                        jeu.investissement_france += difference_position * jeu.Entreprise_Francaise.prix_action
                    if slider_info['entreprise'] == "Russie":
                        jeu.investissement_russie += difference_position * jeu.Entreprise_Russe.prix_action
                    if slider_info['entreprise'] == "Royaume-Uni":
                        jeu.investissement_RU += difference_position * jeu.Entreprise_Britannique.prix_action

                # CONDITION: diminue la valeur d'investissement si défilement de la barre vers la gauche
                elif difference_position >= distance_minimale and \
                slider_info['barre_position_precedente'] > slider_info['barre_position']:
                    # Vérifie quels sliders est déplacés
                    if slider_info['entreprise'] == "Etats-Unis":
                        jeu.investissement_EU -= difference_position * jeu.Entreprise_Americaine.prix_action
                    if slider_info['entreprise'] == "Japon":
                        jeu.investissement_japon -= difference_position * jeu.Entreprise_Japonais.prix_action
                    if slider_info['entreprise'] == "Chine":
                        jeu.investissement_chine -= difference_position * jeu.Entreprise_Chinoise.prix_action
                    if slider_info['entreprise'] == "France":
                        jeu.investissement_france -= difference_position * jeu.Entreprise_Francaise.prix_action
                    if slider_info['entreprise'] == "Russie":
                        jeu.investissement_russie -= difference_position * jeu.Entreprise_Russe.prix_action
                    if slider_info['entreprise'] == "Royaume-Uni":
                        jeu.investissement_RU -= difference_position * jeu.Entreprise_Britannique.prix_action

        # EVENEMENT : si l'utilisateur survole un bouton survolable
        if event.type == pygame_gui.UI_BUTTON_ON_HOVERED:
            if event.ui_element in jeu.systeme.detail_dico:
                bouton_info = jeu.systeme.detail_dico[event.ui_element]
                # Vérifie quels boutons est survolés pour remplacer le logo de l'entreprise associée par ses caractéristiques
                if bouton_info["id_d"] == 1:
                    jeu.description_entreprise = f"{jeu.Entreprise_Americaine.nom_entreprise}\n" \
                                                 f"Vente : {jeu.Entreprise_Americaine.produit}\n" \
                                                 f"Prix : {jeu.Entreprise_Americaine.prix_action}€\n" \
                                                 f"Avis : {jeu.Entreprise_Americaine.note} étoiles\n" \
                                                 f"Description :\n" \
                                                 f"{jeu.Entreprise_Americaine.description}"

                    jeu.logo_entreprise_Americaine = None

                if bouton_info["id_d"] == 2:
                    jeu.description_entreprise = f"{jeu.Entreprise_Japonais.nom_entreprise}\n" \
                                                 f"Vente : {jeu.Entreprise_Japonais.produit}\n" \
                                                 f"Prix : {jeu.Entreprise_Japonais.prix_action}€\n" \
                                                 f"Avis : {jeu.Entreprise_Japonais.note} étoiles\n" \
                                                 f"Description :\n" \
                                                 f"{jeu.Entreprise_Japonais.description}"
                    jeu.logo_entreprise_Japonaise = None

                if bouton_info["id_d"] == 3:
                    jeu.description_entreprise = f"{jeu.Entreprise_Chinoise.nom_entreprise}\n" \
                                                 f"Vente : {jeu.Entreprise_Chinoise.produit}\n" \
                                                 f"Prix : {jeu.Entreprise_Chinoise.prix_action}€\n" \
                                                 f"Avis : {jeu.Entreprise_Chinoise.note} étoiles\n" \
                                                 f"Description :\n" \
                                                 f"{jeu.Entreprise_Chinoise.description}"
                    jeu.logo_entreprise_Chinoise = None

                if bouton_info["id_d"] == 4:
                    jeu.description_entreprise = f"{jeu.Entreprise_Francaise.nom_entreprise}\n" \
                                                 f"Vente : {jeu.Entreprise_Francaise.produit}\n" \
                                                 f"Prix : {jeu.Entreprise_Francaise.prix_action}€\n" \
                                                 f"Avis : {jeu.Entreprise_Francaise.note} étoiles\n" \
                                                 f"Description :\n" \
                                                 f"{jeu.Entreprise_Francaise.description}"
                    jeu.logo_entreprise_Francaise = None

                if bouton_info["id_d"] == 5:
                    jeu.description_entreprise = f"{jeu.Entreprise_Russe.nom_entreprise}\n" \
                                                 f"Vente : {jeu.Entreprise_Russe.produit}\n" \
                                                 f"Prix : {jeu.Entreprise_Russe.prix_action}€\n" \
                                                 f"Avis : {jeu.Entreprise_Russe.note} étoiles\n" \
                                                 f"Description :\n" \
                                                 f"{jeu.Entreprise_Russe.description}"
                    jeu.logo_entreprise_Russe = None

                if bouton_info["id_d"] == 6:
                    jeu.description_entreprise = f"{jeu.Entreprise_Britannique.nom_entreprise}\n" \
                                                 f"Vente : {jeu.Entreprise_Britannique.produit}\n" \
                                                 f"Prix : {jeu.Entreprise_Britannique.prix_action}€\n" \
                                                 f"Avis : {jeu.Entreprise_Britannique.note} étoiles\n" \
                                                 f"Description :\n" \
                                                 f"{jeu.Entreprise_Britannique.description}"
                    jeu.logo_entreprise_Britannique = None

        # EVENEMENT : si l'utilisateur arrete de survoler un bouton survolable
        if event.type ==pygame_gui.UI_BUTTON_ON_UNHOVERED:
            if event.ui_element in jeu.systeme.detail_dico:
                # Arrête l'affichage des descriptions d'entreprises pour afficher leurs logos
                jeu.description_entreprise = None
                jeu.logo_entreprise_Americaine = pygame.image.load("./image/logo_EU.png").convert()
                jeu.logo_entreprise_Japonaise = pygame.image.load("./image/logo_japon.png").convert()
                jeu.logo_entreprise_Chinoise = pygame.image.load("./image/logo_chine.webp").convert()
                jeu.logo_entreprise_Francaise = pygame.image.load("./image/logo_france.png").convert()
                jeu.logo_entreprise_Russe = pygame.image.load("./image/logo_russie.png").convert()
                jeu.logo_entreprise_Britannique = pygame.image.load("./image/logo_RU.png").convert()

        # EVENEMENT : si l'utilisateur fait un clique gauche
        if event.type == pygame.MOUSEBUTTONDOWN:
            x_souris, y_souris = pygame.mouse.get_pos()
            jeu.systeme.check_bouton_if_click(x_souris, y_souris)

        manager_ui.process_events(event)

    manager_ui.update(clock.tick(60)/1000.0) # FPS 60 images par seconde

    # AFFICHAGES des interfaces selon la variable interface_actuelle
    if jeu.interface_actuelle == "Accueille":
        jeu.Interface_Acceuil()
    elif jeu.interface_actuelle == "interface principal":
        jeu.Interface_Principal()
    elif jeu.interface_actuelle == "NEWS":
        jeu.Interface_NEWS()
    elif jeu.interface_actuelle == "Investissement":
        jeu.Interface_Invest()
    elif jeu.interface_actuelle == "Gain et Perte du mois":
        jeu.Interface_Rendu()
    elif jeu.interface_actuelle == "END":
        jeu.Interface_Finale()

    # Dessiner le gestionnaire d'interface utilisateur
    manager_ui.draw_ui(fenetre)
    pygame.display.update()